<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class FriendshipsController extends Controller
{
    public function check($id){

    	if(Auth::user()->isFriendsWith($id) === 1){

    		return ['status' => 'friends'];
    	}

    	if(Auth::user()->hasPendingFriendRequestFrom($id)){

    		return ['status' => 'pending'];
    	}

    	if(Auth::user()->hasPendingFriendRequestSentTo($id)){

    		return ['status' => 'waiting'];
    	}

    	return ['status' => 0];
    }

    public function addFriend($id){

    	$resp = Auth::user()->addFriend($id);

    	User::find($id)->notify(new \App\Notifications\NewFriendRequest(Auth::user()) );

    	return $resp;
    }

    public function acceptFriend($id){

    	return Auth::user()->acceptFriend($id);
    }
}
