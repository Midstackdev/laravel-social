<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    
    Route::get('profile/{slug}', ['as'=>'profile', 'uses'=>'ProfilesController@index']);

    Route::get('profile/edit/me', ['as'=>'profile.edit', 'uses'=>'ProfilesController@edit']);

    Route::post('profile/update/me', ['as'=>'profile.update', 'uses'=>'ProfilesController@update']);

    Route::get('/check_relationship_status/{id}', ['as'=>'check', 'uses'=>'FriendshipsController@check']);

    Route::get('/add_friend/{id}', ['as'=>'add.friend', 'uses'=>'FriendshipsController@addFriend']);

    Route::get('/accept_friend/{id}', ['as'=>'accept.friend', 'uses'=>'FriendshipsController@acceptFriend']);
});
