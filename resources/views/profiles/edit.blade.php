@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit your pforile</div>

                <div class="card-body">
                    <form action="{{ route('profile.update')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label for="avatar">{{ __('Upload Avatar') }}</label>
                            <div class="">
                                <input id="avatar" type="file" class="form-control" name="avatar" accept="image/*">   
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="location">{{ __('Location') }}</label>
                            <div class="">
                                <input id="location" type="text" class="form-control" name="location" value="{{ $info->location }}">   
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="location">{{ __('About me') }}</label>
                            <div class="">
                                <textarea id="about" type="text" class="form-control" name="about">{{ $info->about }}</textarea>   
                            </div>
                        </div>

                        <div class="form-group">
                            <p class="text-center">
                                <button type="submit" class="btn btn-info">Save your info</button>
                            </p>
                        </div>
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
