@extends('layouts.app')


@section('content')

	<div class="container">
		<div class="col-lg-4">
			<div class="card">
				<div class="card-header">
					<p class="text-center">
						{{ $user->name }}'s profile
					</p>
				</div>
				<div class="card-body">
					<center>
						<img src="{{ Storage::url($user->avatar)}}" alt="" height="140px" width="140px" style="border-radius: 50%;" class="mb-3">
					</center>
					<p class="text-center">
						{{ $user->profile->location}}
					</p>

					<p class="text-center">
						@if(Auth::id() == $user->id)
							<a href="{{ route('profile.edit') }}" class="btn btn-info">Edit your profile</a>
						@endif
					</p>
				</div>
			</div>
			<br>
			@if(Auth::id() !== $user->id)
				<div class="card">
					<div class="card-body">
						<friend :profile_user_id="{{ $user->id }}"></friend>
					</div>
				</div>
			@endif	
			<br>
			<div class="card">
				<div class="card-header">
					<p class="text-center">
						About me.
					</p>
				</div>
				<div class="card-body">
					<p class="text-center">
						{{ $user->profile->about }}
					</p>
				</div>
			</div>
		</div>
	</div>








@stop